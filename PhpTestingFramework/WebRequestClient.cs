﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace PhpTestingFramework
{
    public static class WebRequestClient
    {
        public static async Task<JObject> PostJson(string uri, string jsonString)
        {
            var stringContent = new StringContent(jsonString);

            using (var client = new HttpClient())
            {
                var response = await client.PostAsync(uri, stringContent);
                var stringResponse = await response.Content.ReadAsStringAsync();
                var dictionaryResponse = JsonConvert.DeserializeObject<JObject>(stringResponse);
                return dictionaryResponse;
            }
        }
    }
}
