﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using PhpTestingFramework;
using System.Threading.Tasks;

namespace ServicesTests
{
    [TestClass]
    public class PortalTests
    {
        [TestMethod]
        public async Task GetProviderIdByZone()
        {
            //arrange
            var uri = "http://ip.siv-01.napgsys.com/services/portal";

            var jsonObject = new {
                id = "5939be825c34f",
	            method = "getProviderIdByZone",
                @params = new
                {
                    apiKey = "4A77E9C0-1D2B-11E0-AB6B-9346DFD72085",
                    zoneId = "1"
                }
            };

            var jsonString = JsonConvert.SerializeObject(jsonObject);

            //act
            var result = await WebRequestClient.PostJson(uri, jsonString);

            //assert
            Assert.AreEqual(result.SelectToken("result").ToString(), "3");
            Assert.AreEqual(result.SelectToken("error").ToString(), "");
        }

        [TestMethod]
        public async Task GetProviderIdByZoneInjection()
        {
            //arrange
            var uri = "http://ip.siv-01.napgsys.com/services/portal";

            var jsonObjectInjection = new
            {
                id = "5939be825c34f",
                method = "getProviderIdByZone",
                @params = new
                {
                    apiKey = "4A77E9C0-1D2B-11E0-AB6B-9346DFD72085",
                    zoneId = "1 AND [INFERENCE]"
                }
            };

            var jsonStringInjection = JsonConvert.SerializeObject(jsonObjectInjection);

            //act
            var resultInjection = await WebRequestClient.PostJson(uri, jsonStringInjection);

            //assert
            Assert.AreEqual(resultInjection.SelectToken("error").ToString(), "");
        }

        [TestMethod]
        public async Task GetCustomerInformationByCorrelationId()
        {
            //arrange
            var uri = "http://ip.siv-01.napgsys.com/services/portal";

            var jsonObject = new
            {
                id = "5939be825c34f",
                method = "getCustomerInformationByCorrelationId",
                @params = new
                {
                    apiKey = "4A77E9C0-1D2B-11E0-AB6B-9346DFD72085",
                    correlationId = "00005b8b-a2a5-4504-8261-e9f6d49a25b8"
                }
            };

            var jsonString = JsonConvert.SerializeObject(jsonObject);

            //act
            var result = await WebRequestClient.PostJson(uri, jsonString);

            //assert
            Assert.AreEqual(result.SelectToken("result").ToString(), "3");
        }
    }
}
